# Shishi README -- Important introductory notes.

Shishi implements the Kerberos network authentication system.  Shishi
can be used to authenticate users in distributed systems, and is most
often used via GSS-API for SSH or via SASL for IMAP/POP3.

The [Shishi web page](https://www.gnu.org/software/shishi/) provides
current information about the project.

# Support

If you need help to use Generic Security Service, or wish to help
others, you are invited to join our mailing list help-gss@gnu.org, see
<https://lists.gnu.org/mailman/listinfo/help-gss>.

# License

The library, applications and self tests (lib/, db/, src/, tests/) are
licensed under the GNU General Public License license version 3 or
later (see COPYING), and the documentation (doc/) is licensed under
the GNU Free Documentation License version 1.3 or later.

Third party files (e.g., lib/gl/ or src/gl/) are distributed here for
convenience, and they have their own respective licenses.

For any copyright year range specified as YYYY-ZZZZ in this package
note that the range specifies every single year in that closed interval.

----------------------------------------------------------------------
Copyright (C) 2002-2022 Simon Josefsson

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
